import os
import re
import tkinter as tk
import logging
import json
import math
import functools
from typing import Optional
import requests
import l10n
import myNotebook as nb
import timeout_session
from theme import theme
from config import config, appname


_ = functools.partial(l10n.Translations.translate, context=__file__)


class PersistentData:
    def __init__(self):
        self.plugin_pretty_name: str = "Cargo Manifest"
        self.plugin_prefix: str = "inutt/cargo-manifest"
        self.session: requests.Session = timeout_session.new_session()
        self.tk_settings: dict = {}
        self.settings: dict = {}

        self.location: Optional[str] = None
        self.cargo_capacity: int = 0
        self.cargo_total: int = 0
        self.cargo: dict = {}
        self.cargo_categories: list = []
        self.first_cargo_update_received: bool = False
        self.cargo_vessel: str = ""

        self.commodity_data: dict = {}
        self.local_bubble: str = None
        self.current_pos = None

        self.crew_payments: dict = {}
        self.sar_payments: dict = {}

this = PersistentData()
this.plugin_name = os.path.basename(os.path.dirname(__file__))
logger = logging.getLogger(f'{appname}.{this.plugin_name}')

COLOUR_DIMMED  = "#702d00"
#COLOUR_RARE    = "#c800ff"
COLOUR_RARE    = "#ffff00"
COLOUR_ILLEGAL = "#ff0000"
COLOUR_MISSION = "#00ccff"


def plugin_start3(plugin_dir: str) -> str:
    # Initialise config settings
    this.settings['api_server'] = config.get_str(f"{this.plugin_prefix}/api_server", default='')
    this.settings['use_location'] = config.get_str(f"{this.plugin_prefix}/use_location", default='none')
    this.settings['include_fleet_carriers'] = config.get_bool(f"{this.plugin_prefix}/include_fleet_carriers", default=True)
    this.settings['rescue_escape_pods'] = config.get_bool(f"{this.plugin_prefix}/rescue_escape_pods", default=True)
    this.settings['price_data_version'] = config.get_str(f"{this.plugin_prefix}/price_data_version", default='none')

    this.tk_settings['api_server'] = tk.StringVar(value=this.settings['api_server'])
    this.tk_settings['use_location'] = tk.StringVar(value=this.settings['use_location'])
    this.tk_settings['include_fleet_carriers'] = tk.BooleanVar(value=this.settings['include_fleet_carriers'])
    this.tk_settings['rescue_escape_pods'] = tk.BooleanVar(value=this.settings['rescue_escape_pods'])
    this.tk_settings['price_data_version'] = tk.StringVar(value=this.settings['price_data_version'])

    store_settings()

    # Check if we have the latest average price data
    r = this.session.get('https://gitlab.com/api/v4/projects/inutt%2Feddn-commodity-price-data/repository/commits')
    logger.debug(
        "Checking latest averaging pricing data version returned HTTP %d %s",
        r.status_code,
        r.reason,
    )
    if r.ok:
        latest_commit_id = r.json()[0]['id']
        logger.debug("Current price data version id : %s", this.settings['price_data_version'])
        logger.debug("Latest price data version id  : %s (%s)", latest_commit_id, r.json()[0]['created_at'])
        if this.settings['price_data_version'] != latest_commit_id:
            logger.debug("Updating pricing data...")
            r = this.session.get('https://gitlab.com/inutt/eddn-commodity-price-data/-/raw/main/average_prices.json')
            if r.ok:
                with open(os.path.join(plugin_dir, 'average_prices.json'), 'w', encoding='utf-8') as file:
                    file.write(r.text)
                this.settings['price_data_version'] = latest_commit_id
                config.set(f"{this.plugin_prefix}/price_data_version", latest_commit_id)

    # Load in the average commodity price data
    logger.debug("Reading commodity data from %s", os.path.join(plugin_dir, 'average_prices.json'))
    with open(os.path.join(plugin_dir, 'average_prices.json'), 'r', encoding='utf-8') as file:
        this.commodity_data = json.load(file)
    if not this.commodity_data:
        logger.error("Couldn't load commodity data")

    return this.plugin_pretty_name

def plugin_stop() -> None:
    this.session.close()

def prefs_changed(cmdr: str, is_beta: bool) -> None:
    for setting, value in this.tk_settings.items():
        this.settings[setting] = value.get()
    store_settings()
    update_status()


def store_settings():
    for setting, value in this.settings.items():
        config.set(f"{this.plugin_prefix}/{setting}", value)


def get_distance_between(start, end):
    x = end[0] - start[0]
    y = end[1] - start[1]
    z = end[2] - start[2]
    return math.sqrt(x*x + y*y + z*z)


def update_nearest_bubble():
    if this.current_pos is None:
        return None

    closest_distance = None
    closest_bubble = None
    for _,bubble in this.commodity_data.items():
        bubble_edge_distance = get_distance_between(this.current_pos, bubble['centre']) - bubble['radius']
        if closest_distance is None or bubble_edge_distance < closest_distance:
            closest_distance = bubble_edge_distance
            closest_bubble = bubble

    this.local_bubble = closest_bubble


def commodity_average_value(commodity_id: str) -> float:
    if this.local_bubble is None:
        return 0

    prices = this.local_bubble['commodities']
    if commodity_id in prices:
        if commodity_id in ['occupiedcryopod', 'damagedescapepod'] and this.settings['rescue_escape_pods']:
            commodity_id = f'sar_{commodity_id}'
        if commodity_id in prices:
            if this.settings['include_fleet_carriers']:
                return prices[commodity_id]['fleet_carriers_included']
            return prices[commodity_id]['fleet_carriers_excluded']
    return 0


def plugin_app(parent):
    frame = tk.Frame(parent)

    this.header = tk.Label(
        master = frame,
        text = f'{_("Cargo manifest")}',
        justify = tk.LEFT,
    )
    this.header.grid(row=0, column=0, sticky=tk.W, columnspan=3)

    this.cargo_manifest_container = tk.Frame(master = frame)
    this.cargo_manifest_container.grid(row=2, column=1, sticky=tk.W)
    this.cargo_manifest_container.columnconfigure(2, weight=1)

    # Placeholder while waiting for a cargo update from the API
    this.placeholder = tk.Label(
        master = this.cargo_manifest_container,
        text = f'    {_("Waiting for full cargo state update")}...',
        justify = tk.LEFT,
        foreground = "#702d00",
    )
    this.placeholder.grid(row=0, column=0, sticky=tk.W, columnspan=3)

    frame.columnconfigure(1, weight=1)

    this.frame = frame
    return frame


def plugin_prefs(parent: nb.Notebook, cmdr: str, is_beta: bool) -> Optional[tk.Frame]:
    # Layout adapted from EDMC's coriolis plugin code
    PADX = 10  # noqa: N806
    PADY = 1  # noqa: N806
    BOXY = 2  # noqa: N806  # box spacing
    vertical_spacing = 20

    frame = nb.Frame(parent)
    frame.columnconfigure(1, weight=1)

    row = 0

    nb.Label(frame, text=_("Price data version")+":").grid(row=row, column=0, sticky=tk.E, padx=PADX, pady=(PADY+vertical_spacing, PADY))
    nb.Label(frame, text=this.settings['price_data_version']).grid(row=row, column=1, sticky=tk.W, padx=PADX, pady=(BOXY+vertical_spacing, BOXY))

    row += 1
    nb.Label(frame, text=_("Limit prices to bubble")+":").grid(row=row, column=0, sticky=tk.E, padx=PADX, pady=(PADY+vertical_spacing, PADY))
    nb.Radiobutton(frame,
        text="None (show galactic average prices)",
        value="none",
        variable=this.tk_settings['use_location'],
    ).grid(row=row, column=1, sticky=tk.W, padx=PADX, pady=(BOXY+vertical_spacing, BOXY))
    row += 1
    nb.Radiobutton(frame,
        text="Use live location (show prices for nearest bubble, or galactic average if too far from marketplaces)",
        value="live",
        variable=this.tk_settings['use_location'],
    ).grid(row=row, column=1, sticky=tk.W, padx=PADX, pady=BOXY)
    row += 1
    nb.Radiobutton(frame,
        text="The Bubble (Centred on Sol)",
        value="Sol",
        variable=this.tk_settings['use_location'],
    ).grid(row=row, column=1, sticky=tk.W, padx=PADX, pady=BOXY)
    row += 1
    nb.Radiobutton(frame,
        text="Colonia Region (Centred on Colonia)",
        value="Colonia",
        variable=this.tk_settings['use_location'],
    ).grid(row=row, column=1, sticky=tk.W, padx=PADX, pady=BOXY)

    row += 1
    nb.Label(frame, text=_("Commodity value adjustments")+":").grid(row=row, column=0, sticky=tk.E, padx=PADX, pady=(PADY+vertical_spacing, PADY))
    nb.Checkbutton(frame,
        text=_("Include fleet carrier markets"),
        variable=this.tk_settings['include_fleet_carriers'],
        onvalue=True,
        offvalue=False,
    ).grid(row=row, column=1, sticky=tk.W, padx=PADX, pady=(BOXY+vertical_spacing, BOXY))

    row += 1
    nb.Checkbutton(frame,
        text=_("Use SAR value for escape pods"),
        variable=this.tk_settings['rescue_escape_pods'],
        onvalue=True,
        offvalue=False,
    ).grid(row=row, column=1, sticky=tk.W, padx=PADX, pady=BOXY)

    row += 1
    nb.Label(frame, text=_("API server")+":").grid(row=row, column=0, sticky=tk.E, padx=PADX, pady=(PADY+vertical_spacing, PADY))
    nb.Entry(frame, textvariable=this.tk_settings['api_server']).grid(row=row, column=1, sticky=tk.W+tk.E, padx=PADX, pady=(BOXY+vertical_spacing, BOXY))
    row += 1
    nb.Label(frame, text=_("Leave blank unless you're running your own API server")).grid(row=row, column=1, sticky=tk.W, padx=PADX, pady=PADY)

    return frame


def process_cargo_data(cargo_data: dict):
    this.cargo.clear()
    this.cargo_categories.clear()
    this.cargo_total = 0

    for cargo in cargo_data:
        cargo_id = cargo['Name'].lower()
        cargo_count = cargo["Count"]
        cargo_name = cargo["Name"]
        cargo_rare = False
        if "Name_Localised" in cargo:
            cargo_name = cargo["Name_Localised"]
        if this.local_bubble is not None and cargo_id in this.local_bubble['commodities']:
            cargo_category = this.local_bubble['commodities'][cargo_id]['category']
            if 'is_rare' in this.local_bubble['commodities'][cargo_id]:
                cargo_rare = this.local_bubble['commodities'][cargo_id]["is_rare"]
        else:
            cargo_category = "other"

        if cargo_category not in this.cargo_categories:
            this.cargo_categories.append(cargo_category)

        this.cargo_total += cargo_count

        if cargo_id in this.cargo:
            this.cargo[cargo_id]["quantity"] += cargo_count
        else:
            this.cargo[cargo_id] = {
                "name": cargo_name,
                "category": cargo_category,
                "quantity": cargo_count,
                "stolen": False,
                "is_rare": cargo_rare,
                "is_mission": "MissionID" in cargo,   # Stores a bool, not the actual mission ID
            }

        # If any of this cargo type is stolen, mark it.
        # NB: Only check for true here, so that any non-stolen cargo doesn't remove the mark again.
        if cargo["Stolen"]:
            this.cargo[cargo_id]["stolen"] = True


def process_sar_messages(sar, crew):
    commodity = sar['Name']
    value = (sar['Reward'] + crew['Amount']) / sar['Count']

    npc_share = {
        'name': crew['NpcCrewName'],
        'paid': crew['Amount'],
        'percentage': 100 * crew['Amount'] / value,
    }
    logger.debug(
        "Received %d for %d x %s, plus %d to %s for value of %d at market %s",
        sar['Reward'],
        sar['Count'],
        sar['Name'],
        crew['Amount'],
        crew['NpcCrewName'],
        (sar['Reward'] + crew['Amount']) / sar['Count'],
        sar['MarketID'],
    )
    this.session.post(f"{this.settings['api_server']}/api/sar/{commodity}", json={
        'timestamp': sar['timestamp'],
        'market_id': sar['MarketID'],
        'value': value,
        'npc': npc_share,
    })


def journal_entry(cmdr, is_beta, system, station, entry, state):
    if 'StarPos' in state:
        if this.current_pos != state['StarPos']:
            this.current_pos = state['StarPos']
            update_nearest_bubble()
            update_status()

    if 'ShipType' in state and state['ShipType'] is not None:
        this.ship_id = state['ShipType']

    # if entry['event'] == 'Cargo':
    #     # The cargo event does not contain any details about mission cargo (the CargoJSON state
    #     # is required for that), so we can't provide full info from this event, despite it
    #     # probably being a better way of detecting cargo changes.
    #     logger.debug(entry)
    #     this.first_cargo_update_received = True
    #     this.cargo_vessel = entry['Vessel']
    #     process_cargo_data(entry['Inventory'])
    #     update_status()

    if 'CargoJSON' in state:
        this.cargo.clear()
        this.cargo_categories.clear()
        this.cargo_total = 0
        this.first_cargo_update_received = True

        if state["CargoJSON"] is not None and "Inventory" in state["CargoJSON"]:
            this.cargo_vessel = state['CargoJSON']['Vessel']
            process_cargo_data(state["CargoJSON"]["Inventory"])
            update_status()


    # Find out how many cargo racks we have and their total capacity
    if 'Modules' in state and state['Modules'] is not None:
        this.cargo_capacity = 0
        for slot in state['Modules']:
            module_id = state["Modules"][slot]["Item"]
            matches = re.search(r"cargorack_size(\d+)", module_id)
            if matches is not None:
                this.cargo_capacity += 2 ** int(matches[1])
        update_status()


    # Search and rescue values aren't generally available via EDDN, so this is a bit of code to
    # submit them to a custom server when items are handed in to the relevant contact.
    #
    # Note that these messages have to be modified to get the actual commodity value as the prices
    # logged in the journal are the payment received with the any NPC crew member(s)'s share already
    # deducted. To do this, we watch for NpcCrewPaidWage messages with the same timestamp as the
    # search and rescue message.
    if this.settings['api_server']:
        if entry['event'] == 'SearchAndRescue':
            if entry['timestamp'] in this.crew_payments:
                process_sar_messages(entry, this.crew_payments.pop(entry['timestamp']))
            else:
                # Store the entry for matching when the corresponding NPC crew paid event arrives
                this.crew_payments[entry['timestamp']] = entry

        if entry['event'] == 'NpcCrewPaidWage':
            # TODO: Handle having multiple NPC crew (they still get paid if inactive)
            if entry['timestamp'] in this.sar_payments:
                process_sar_messages(this.sar_payments.pop(entry['timestamp']), entry)
            else:
                # Store the entry for matching when the corresponding SAR event arrives
                this.crew_payments[entry['timestamp']] = entry


def update_status() -> None:
    if not this.first_cargo_update_received:
        return

    # Remove existing cargo list
    for widget in this.cargo_manifest_container.winfo_children():
        widget.destroy()

    i = 0

    if len(this.cargo) == 0:
        # Placeholder when no cargo is present
        this.placeholder = tk.Label(
            master = this.cargo_manifest_container,
            text = f'    {_("No cargo detected")}',
            justify = tk.LEFT,
            foreground = "#702d00",
        )
        this.placeholder.grid(row=i, column=0, sticky=tk.W, columnspan=3)
        i += 1

    else:
        # Add new list
        total_cargo_value = 0
        this.cargo_categories.sort()

        for category in this.cargo_categories:
            cargo_header = tk.Label(
                master = this.cargo_manifest_container,
                text = _(category.capitalize()),
                justify = tk.LEFT,
                foreground = "#702d00",
            )
            cargo_header.grid(row=i, column=0, sticky=tk.W, columnspan=5)
            i += 1

            for cargo_type in sorted(this.cargo):
                cargo = this.cargo[cargo_type]

                if cargo["category"] != category:
                    continue

                cargo_padding = tk.Label(
                    master = this.cargo_manifest_container,
                    text = "    ",
                    justify = tk.LEFT,
                    borderwidth = 0,
                    highlightthickness = 0,
                    padx = 5,
                    pady = 0,
                )
                cargo_padding.grid(row=i, column=0, sticky=tk.W)

                cargo_quantity = tk.Label(
                    master = this.cargo_manifest_container,
                    text = f"{cargo['quantity']} \u00d7",
                    justify = tk.RIGHT,
                    borderwidth = 0,
                    highlightthickness = 0,
                    padx = 5,
                    pady = 0,
                )
                cargo_quantity.grid(row=i, column=1, sticky=tk.E)

                cargo_name = tk.Label(
                    master = this.cargo_manifest_container,
                    text = cargo["name"],
                    justify = tk.LEFT,
                    borderwidth = 0,
                    highlightthickness = 0,
                    padx = 5,
                    pady = 0,
                )
                if cargo["is_rare"]:
                    cargo_name.config(foreground = COLOUR_RARE)
                if cargo["stolen"]:
                    cargo_name.config(text = f'{cargo["name"]} {_("[!!]")}')
                    cargo_name.config(foreground = COLOUR_ILLEGAL)
                if cargo["is_mission"]:
                    cargo_name.config(foreground = COLOUR_MISSION)
                cargo_name.grid(row=i, column=2, sticky=tk.W)

                cargo_price = round(commodity_average_value(cargo_type))

                if cargo_price > 0:
                    cargo_value = tk.Label(
                        master = this.cargo_manifest_container,
                        text = f"{l10n.Locale.string_from_number(cargo_price, 0)} {_('cr')}",
                        justify = tk.RIGHT,
                        borderwidth = 0,
                        highlightthickness = 0,
                        padx = 10,
                        pady = 0,
                    )
                    cargo_value.grid(row=i, column=3, sticky=tk.E)

                    cargo_value_total = tk.Label(
                        master = this.cargo_manifest_container,
                        text = f"{l10n.Locale.string_from_number(cargo_price * cargo['quantity'], 0)} {_('cr')}",
                        justify = tk.RIGHT,
                        foreground = COLOUR_DIMMED,
                        borderwidth = 0,
                        highlightthickness = 0,
                        padx = 10,
                        pady = 0,
                    )
                    cargo_value_total.grid(row=i, column=4, sticky=tk.E)

                    total_cargo_value += cargo_price * cargo["quantity"]

                i += 1

        if total_cargo_value > 0 and this.cargo_capacity > 0:
            label_text = _("Approximate total value at galactic average prices")
            if this.local_bubble is not None:
                label_text = _("Approximate total value in {BUBBLE_NAME}").format(BUBBLE_NAME=this.local_bubble['name'])
            cargo_footer = tk.Label(
                master = this.cargo_manifest_container,
                text = label_text,
                justify = tk.LEFT,
                foreground = COLOUR_DIMMED,
                padx = 5,
                pady = 0,
            )
            cargo_footer.grid(row=i, column=2, sticky=tk.E, columnspan=2)

            cargo_footer = tk.Label(
                master = this.cargo_manifest_container,
                text = f"{l10n.Locale.string_from_number(total_cargo_value, 0)} {_('cr')}",
                justify = tk.RIGHT,
                borderwidth = 0,
                highlightthickness = 0,
                padx = 10,
                pady = 0,
            )
            cargo_footer.grid(row=i, column=4, sticky=tk.E)

            i += 1

    if this.cargo_capacity > 0:
        remaining_percentage = 100 - int(this.cargo_total * 100 / this.cargo_capacity)
        cargo_footer = tk.Label(
            master = this.cargo_manifest_container,
            text = f'{_("Remaining capacity")}: {this.cargo_capacity - this.cargo_total} {_("tonnes")} ({remaining_percentage}%)',
            justify = tk.LEFT,
        )
        cargo_footer.grid(row=i, column=0, sticky=tk.W, columnspan=5)

    else:
        cargo_padding = tk.Label(
            master = this.cargo_manifest_container,
            text = "    ",
            justify = tk.LEFT,
            borderwidth = 0,
            highlightthickness = 0,
            padx = 5,
            pady = 0,
        )
        cargo_padding.grid(row=i, column=0, sticky=tk.W)

        cargo_footer = tk.Label(
            master = this.cargo_manifest_container,
            text = f'{_("No cargo racks found")}',
            justify = tk.LEFT,
            foreground = COLOUR_DIMMED,
        )
        cargo_footer.grid(row=i, column=1, sticky=tk.W, columnspan=4)

    # Make sure the widgets are themed correctly
    theme.update(this.cargo_manifest_container)

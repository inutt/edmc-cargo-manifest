# EDMC Cargo Manifest plugin

A plugin for [Elite Dangerous Market
Connector](https://github.com/EDCD/EDMarketConnector/wiki) to show your current
cargo and its approximate value at a glance, inspired by
[RemainNA's cargo manifest](https://github.com/RemainNA/cargo-manifest).


## Screenshot

![Screenshot of manifest display](img/manifest.png)

1.  Category of cargo
2.  Name of cargo
3.  Quantity of each cargo type in cargo hold
4.  Value per unit
5.  Total value for each cargo type
6.  Approximate total value of all cargo at local average prices
7.  Remaining cargo capacity

If a cargo type does not have a price shown, as is the case for 'Limpets' in the
above screenshot, it means either that the item is not a tradable cargo, or
prices for it are not currently known.


## Installation/Updating

Download the latest release zip from the releases tab, and unpack it into your
EDMC plugins directory. (The plugins tab in EDMC's settings has a button to open
the appropriate directory, or see [EDMC's plugin
documentation](https://github.com/EDCD/EDMarketConnector/wiki/Plugins#more-info)
for how to locate it manually). You can also just download `load.py` to the
appropriate location if it's been updated and there hasn't been a new release
yet.

Your plugins directory should now have a directory called `edmc-cargo-manifest`,
which should contain some or all of the files from this repository. The file
`load.py` is required as it contains the plugin code, `average_prices.json` will
be automatically downloaded when the plugin loads, and other files should not
affect anything.

![Screenshot of correct file layout in Windows Explorer](img/files_windows.png)


## Configuration

![Screenshot of configuration window](img/config.png)

1.  Identifier for the version of the commodity price data file. For information
    only as the price data should be updated automatically.
2.  Options to select which subset of pricing data to use. **Note: This feature
    is incomplete, and these options currently do nothing.**
3.  Options to adjust which prices are used. Specifically, whether to include or
    exclude fleet carriers from the average prices, and whether to use search
    and rescue (SAR) or black market prices for escape pods.
4.  The URI of a custom API server if you're running your own instance of the
    [EDDN listener](https://gitlab.com/inutt/eddn-listener) and want to submit
    search and rescue values to it. You almost certainly won't need to enter
    anything here.


## FAQ

### Why do I see "Waiting for full cargo state update..."?

This means the game has not yet provided a full cargo list, and will usually be
shown between (re-)starting the game or EDMC and the first cargo update. Full
updates are usually generated if your cargo inventory changes or when you dock
at a station.


### What do the different colours mean for the cargo name?

I should have put this in the screenshot, but forgot. Orange is the default, red
(also with `[!!]` after the name) means that cargo is illegal/stolen, and blue
indicates a cargo required for an active mission.


## Note about data source

Average cargo prices are obtained from a [separate
repository](https://gitlab.com/inutt/eddn-commodity-price-data), which is the
result of a listener process analysing messages sent to the community run [Elite
Dangerous Data Network](https://github.com/EDCD/EDDN).

**You do not need to run the listener process yourself** - this plugin will
automatically retrieve the latest pricing data from the pricing repository and
keep it up to date as necessary. In the event that I'm no longer updating that
repository for any reason, you can use the code
[here](https://gitlab.com/inutt/eddn-listener) to generate your own pricing
data.
